import Parent from "./components/Parent"
function App() {
  return (
    <div className='container mt-5 text-center'>
      <Parent></Parent>
    </div>
  );
}

export default App;

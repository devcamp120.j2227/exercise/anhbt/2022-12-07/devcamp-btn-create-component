import { Component } from "react";
import Display from "./Display";

class Parent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: false
        }
    }
    updateDisplay = () => {
        this.setState({
            display : !this.state.display
        })
    }

    render() {
        return (
            <div>
                <button 
                className = {this.state.display ? "btn btn-danger" : "btn btn-info"} 
                onClick = {this.updateDisplay}>
                    {this.state.display ? "Destroy Component" : "Create Component"}
                </button>
                { this.state.display ? <Display /> : null }
            </div>
        )
    }
}

export default Parent;
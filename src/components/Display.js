import { Component } from "react";

class Display extends Component{
    componentWillMount() {
        console.log("component will mount");
    }

    componentDidMount() {
        console.log("component did mount");
    }

    componentWillUnmount() {
        console.log("component will unmount");
    }
    
    render() {
        console.log('render');

        return(
            <h1>I exist !</h1>
        )
    }
}
export default Display;